import bs4
import requests
import xlsxwriter

url = 'https://www.flanco.ro/laptop-it-tablete/laptop.html?gclid=CjwKCAiA-9uNBhBTEiwAN3IlNGcygTDTo70nO3FaowlxGgZ6seFKD8yjk1MClTl12BdyRzSTScuEkhoCO5EQAvD_BwE'
result = requests.get(url)
soup = bs4.BeautifulSoup(result.text, 'lxml')
cases = soup.find_all('div', class_='product-item-info')
context = {'data': []}
for case in cases:
    data = {}
    product_name = case.find('a', class_='product-item-link')
    product_price = case.find('span', class_='price')
    data['product_name'] = product_name.text
    data['product_price'] = product_price.text
    context['data'].append(data)

workbook = xlsxwriter.Workbook('Laptopuri.xlsx')
worksheet = workbook.add_worksheet('Laptopuri de pe flanco')

row = 0
col = 0
for i in context['data']:
    worksheet.write(row, col, i['product_name'])
    worksheet.write(row, col + 1, i['product_price'])
    row += 1

workbook.close()
