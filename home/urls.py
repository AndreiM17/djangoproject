from django.urls import path

from home import views

urlpatterns = [
    path('page/', views.page, name='page'),
    path('all_students/', views.home, name='list_of_students'),
    path('all_cars/', views.brands, name='list_of_cars'),
    path('', views.HomeTemplateView.as_view(), name='homepage')
]
