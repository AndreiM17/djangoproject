from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView


def page(request):
    return HttpResponse('Hello, Andrei')


@login_required
def home(request):
    context = {
        'all_students': [
            {
                'first_name': 'Andrei',
                'last_name': 'Marinoiu',
                'age': 31
            },
            {
                'first_name': 'Mircea',
                'last_name': 'Saliste',
                'age': 26
            }
        ]
    }
    return render(request, 'home/home.html', context)


def brands(request):
    context = {
        'all_cars': [
            {
                'brand': 'Ferrari',
                'model': 'SF90',
                'year': 2019
            },
            {
                'brand': 'Porsche',
                'model': '911',
                'year': 1989
            }
        ]
    }
    return render(request, 'home/brands.html', context)


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'
