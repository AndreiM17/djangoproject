from django.shortcuts import render

from teacher.models import Teacher


def get_all_teachers_per_course(request, id_teacher):
    all_teachers_per_course = Teacher.objects.filter(id=id_teacher)
    context = {'get_all_teachers': all_teachers_per_course}
    return render(request, 'course/get_all_teachers_per_course.html', context)
