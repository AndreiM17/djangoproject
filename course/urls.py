from django.urls import path

from course import views

urlpatterns = [
    path('teachers_per_course/<int:id_teacher>/', views.get_all_teachers_per_course, name='teachers-per-course')
]
