from django.db import models


class Animal(models.Model):
    race = models.CharField(max_length=30)
    color = models.CharField(max_length=30)
    active = models.BooleanField(default=True)
    create_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.race} {self.color}'
