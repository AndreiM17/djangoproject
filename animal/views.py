from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView

from animal.forms import AnimalForm
from animal.models import Animal


class AnimalCreateView(CreateView):
    template_name = 'animal/create_animal.html'
    model = Animal
    success_url = reverse_lazy('create-animal')
    form_class = AnimalForm
