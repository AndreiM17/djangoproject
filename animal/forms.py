from django import forms
from django.forms import TextInput

from animal.models import Animal


class AnimalForm(forms.ModelForm):
    class Meta:
        model = Animal
        # fields = '__all__'    # campurile pe care le vrem in formular
        fields = ['race', 'color']
        widgets = {
            'race': TextInput(attrs={'placeholder': 'Please enter animal race', 'class': 'form-control'}),
            'color': TextInput(attrs={'placeholder': 'Please enter animal color', 'class': 'form-control'})

        }
