from django.urls import path

from animal import views

urlpatterns = [
    path('create_animal/', views.AnimalCreateView.as_view(), name='create-animal')
]
