from course.models import Course
from teacher.models import Teacher


def get_all_teachers(request):
    all_teachers = Teacher.objects.all()  # querry set
    return {'teachers': all_teachers}


# randeaza in orice fisier html datele

def get_all_courses(request):
    all_courses = Course.objects.all()
    return {'courses': all_courses}
