from django import forms
from django.forms import TextInput

from teacher.models import Teacher


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        # fields = '__all__'    # campurile pe care le vrem in formular
        fields = ['first_name', 'last_name', 'specialization']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your last name', 'class': 'form-control'}),
            'specialization': TextInput(attrs={'placeholder': 'Please enter your specialization',
                                               'class': 'form-control'})

        }
