# Generated by Django 3.2.9 on 2021-11-24 18:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teacher', '0003_teacher'),
    ]

    operations = [
        migrations.RenameField(
            model_name='teacher',
            old_name='specialisation',
            new_name='specialization',
        ),
    ]
