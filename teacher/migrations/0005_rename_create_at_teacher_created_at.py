# Generated by Django 3.2.9 on 2021-11-24 18:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teacher', '0004_rename_specialisation_teacher_specialization'),
    ]

    operations = [
        migrations.RenameField(
            model_name='teacher',
            old_name='create_at',
            new_name='created_at',
        ),
    ]
