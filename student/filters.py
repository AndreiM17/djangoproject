import django_filters

from student.models import Student


class StudentFilters(django_filters.FilterSet):
    class Meta:
        model = Student # tabela unde sunt stocate datele
        fields = ['first_name', 'last_name', 'age', 'date_of_birth', 'olympic', 'gender', 'teacher', 'active',
                  'created_at']
